import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { EMPLOYEES } from '../models/mock-employees';
import { Employee } from '../models/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor() { }

  getEmployees(): Observable<Employee[]> {
    return of(EMPLOYEES);
  }

  getEmployee(id: number): Observable<Employee> {
    return of(EMPLOYEES.find(employee => employee.id === id));
  }

  createEmployee(employee: Employee): Observable<any>{
    return of(EMPLOYEES.push(employee));
  }

}
