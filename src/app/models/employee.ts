export class Employee {
    id: number;
    lastname: string;
    firstname: string;
    firstSalary: number;
    increase?: number[];
    missions?: string[];
  }