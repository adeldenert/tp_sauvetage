import { Employee } from './employee';

export const EMPLOYEES: Employee[] = [
    {
        id: 1,
        lastname: 'Denert',
        firstname: 'Adel',
        firstSalary: 1200,
        increase: [200, 200, 200],
        missions: ['Formation', 'Projet angular fail', 'Tp sauvetage']
    },
    {
        id: 2,
        lastname: 'Terieur',
        firstname: 'Alain',
        firstSalary: 700,
        increase: [100, 200],
        missions: ['Mission impossible', 'Mission impossible 2']
    },
    {
        id: 3,
        lastname: 'Bono',
        firstname: 'Jean',
        firstSalary: 830,
        increase: [50, 160, 280],
        missions: ['Couper du jambon', 'Manger du jambon']
    }
];