import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Employee } from '../models/employee';
import { EmployeeService } from '../services/employee.service';


@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.scss']
})
export class EmployeeDetailComponent implements OnInit {

  employee: Employee;

  constructor(private employeeService: EmployeeService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.getEmployee();
  }

  getSalary(firstSalary, increase){
    let salary = firstSalary;
    increase.forEach(element => {
      salary += element;
    });
    return salary;
  }

  getEmployee(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.employeeService.getEmployee(id)
      .subscribe(employee => this.employee = employee);
  }

}
