import { Component, OnInit } from '@angular/core';

import { EmployeeService } from '../services/employee.service';
import { Employee } from '../models/employee';

import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

  creationForm = new FormGroup({
    id: new FormControl(''),
    lastname: new FormControl(''),
    firstname: new FormControl(''),
    firstSalary: new FormControl('')
  });


  employees: Employee[];
  displayedColumns: string[] = ['id', 'lastname', 'firstname', 'show'];

  creationMode = false;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.employeeService.getEmployees().subscribe(employees => this.employees = employees);
  }

  displayForm(){
    this.creationMode = true;
  }

  onSubmit(){
    let employee = new Employee;
    employee.id = this.creationForm.value.id;
    employee.lastname = this.creationForm.value.lastname;
    employee.firstname = this.creationForm.value.firstname;
    employee.firstSalary = this.creationForm.value.firstSalary;

    this.employeeService.createEmployee(employee).subscribe();

    this.creationMode = false;
  }

}
